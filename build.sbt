organization := "io.github.waleedsamy"

name := "ss-crawler"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.12"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Dependencies.all

enablePlugins(ScalatraPlugin)
