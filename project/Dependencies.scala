import sbt._

object Dependencies {
  object Version {
    val ScalatraVersion                 = "2.6.3"
    val json4sJackson                   = "3.5.2"
    val logback                         = "1.2.3"
    val servlet                         = "3.1.0"
    val config                          = "1.3.1"
    val scalaScraper                    = "2.1.0"
    val apacheCodec                     = "1.10"
    val commonsNet                      = "3.5"
    val jetty                           = "9.4.9.v20180320"
  }

  object Compile {
    val scalatraCore          = "org.scalatra"      %% "scalatra"         % Version.ScalatraVersion
    val scalatraJson          = "org.scalatra"      %% "scalatra-json"    % Version.ScalatraVersion
    val json4sJackson         =  "org.json4s"       %% "json4s-jackson"   % Version.json4sJackson
    val scraper               = "net.ruippeixotog"  %% "scala-scraper"    % Version.scalaScraper
    val logbackClassic        =  "ch.qos.logback"   % "logback-classic"   % Version.logback
    val jetty                 = "org.eclipse.jetty" % "jetty-webapp"      % Version.jetty % "container"
    val servlet               = "javax.servlet"     % "javax.servlet-api" % Version.servlet % "provided"
    val config                = "com.typesafe"      % "config"            % Version.config
    val commonsNet            = "commons-net"       % "commons-net"       % Version.commonsNet
  }

  object Test {
    val specs2Core            = "org.specs2" %% "specs2-core"            % Version.ScalatraVersion  % "test"
    val scalatraSpecs2        = "org.scalatra" %% "scalatra-specs2"      % Version.ScalatraVersion  % "test"
    val specs2Mock            = "org.specs2"   %% "specs2-mock"          % Version.ScalatraVersion  % "test"
    val specs2Matcher         = "org.specs2"   %% "specs2-matcher-extra" % Version.ScalatraVersion  % "test"
  }

  import Compile._
  private val logging = Seq(logbackClassic)
  private val testing = Seq(Test.scalatraSpecs2, Test.specs2Mock, Test.specs2Matcher)

  val core = Seq(scalatraCore, scalatraJson, json4sJackson, jetty,  servlet, config, commonsNet, scraper) ++ logging

  val all = core ++ testing
}
