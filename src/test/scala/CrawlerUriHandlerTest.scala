package io.github.waleedsamy.sscrawler

import java.net.HttpURLConnection

import io.github.waleedsamy.sscrawler.crawler.{ CrawlerController, CrawlerUriHandler }
import io.github.waleedsamy.sscrawler.model.{ IndexRequest, IndexResponse }
import org.json4s.jackson.Serialization._
import org.mockito.Mockito
import org.scalatra.test.specs2.MutableScalatraSpec
import org.specs2.concurrent.ExecutionEnv
import org.specs2.mock.{ Mockito => Specs2Mockito }

import scala.concurrent.Future

class CrawlerUriHandlerTest extends TestBase with MutableScalatraSpec with Specs2Mockito {

  def is(implicit executionEnv: ExecutionEnv): Unit = {

    val controller = mock[CrawlerController]

    def executionContext = executionEnv.executionContext

    addServlet(new CrawlerUriHandler(executionContext, controller), CrawlerUriHandler.Path)

    sequential

    "POST /index" >> {
      def setupMocks() = {
        Mockito.reset(controller)
        val mockResult = Future.successful(SampleIndexResponse)
        controller.indexPage(any[IndexRequest]).returns(mockResult)
      }

      sequential
      "should call controller.indexPage()" >> {
        setupMocks()
        post("/index", write(SampleIndexRequest)) {
          there was one(controller).indexPage(SampleIndexRequest)
        }
      }
      "should return status 200 Created on success" >> {
        setupMocks()
        post("/index", write(SampleIndexRequest)) {
          status must_== HttpURLConnection.HTTP_OK
        }
      }
      "should return a valid json object on success" >> {
        setupMocks()
        post("/index", write(SampleIndexRequest)) {
          IndexResponse.readOpt(response.body).isDefined must beTrue
        }
      }

      "should return 400 Bad Request on invalid json data without calling the controller" >> {
        setupMocks()
        val invalidJson = "{}"
        post("/index", invalidJson) {
          there was no(controller).indexPage(any[IndexRequest])
          status must_== HttpURLConnection.HTTP_BAD_REQUEST
        }
      }
    }
  }
}