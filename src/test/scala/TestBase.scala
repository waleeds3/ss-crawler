package io.github.waleedsamy.sscrawler

import io.github.waleedsamy.sscrawler.model.{ IndexRequest, IndexResponse, IndexedPage, LowerCaseContent }
import io.github.waleedsamy.sscrawler.shared.Logging
import org.json4s.{ DefaultFormats, Formats }
import org.specs2.mutable.Specification

trait TestBase extends Specification with Logging {

  protected implicit val jsonFormats: Formats = DefaultFormats

  val SampleIndexRequest = IndexRequest("https://place.de")
  val SampleIndexResponse = IndexResponse(SampleIndexRequest)

  val SampleIndexedPage = IndexedPage(
    host = "nowhere.de",
    uri = "https://nowhere.de",
    body = new LowerCaseContent("Hello World")
  )
}
