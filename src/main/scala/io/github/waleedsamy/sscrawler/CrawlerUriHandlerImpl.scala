package io.github.waleedsamy.sscrawler

import io.github.waleedsamy.sscrawler.shared.Logging
import org.json4s.MappingException
import org.scalatra._

import scala.concurrent.{ ExecutionContext, Future }

trait CrawlerUriHandlerImpl extends ScalatraBase with Logging {

  protected implicit def executor: ExecutionContext

  protected def postLogRecover(transformers: RouteTransformer*)(handler: => Future[ActionResult]): Route = {
    post(transformers: _*) {
      new AsyncResult {
        val is = {
          log.info(s"handling POST request: ${request.getRequestURI}")
          log.info(s"params: $params")
          handler.recover(errorHandlers)
        }
      }
    }
  }

  protected def getLogRecover(transformers: RouteTransformer*)(handler: => Future[ActionResult]): Route = {
    get(transformers: _*) {
      new AsyncResult {
        val is = {
          log.info(s"handling GET request: ${request.getRequestURI}")
          log.info(s"params: $params")
          handler.recover(errorHandlers)
        }
      }
    }
  }

  protected def errorHandlers: PartialFunction[Throwable, ActionResult] = {
    case ex: IllegalArgumentException =>
      logInputError(ex); BadRequest(ex.getMessage)
    case ex: MappingException =>
      logInputError(ex); BadRequest(ex.getMessage)
    case ex: IndexOutOfBoundsException =>
      logInputError(ex); NotFound(ex.getMessage)
    case ex => log.error(ex.getMessage, ex); InternalServerError(ex.getMessage)
  }

  protected def logInputError(ex: Throwable) =
    log.info(s"Common input error handled: ${ex.getClass.getName}: ${ex.getMessage}")
}