package io.github.waleedsamy.sscrawler.crawler

import io.github.waleedsamy.sscrawler.model.IndexRequest
import io.github.waleedsamy.sscrawler.{ CrawlerServiceStack, CrawlerUriHandlerImpl }
import org.json4s.{ DefaultFormats, Formats }
import org.scalatra.Created

import scala.concurrent.ExecutionContext

class CrawlerUriHandler(
    executionContext: ExecutionContext,
    controller: CrawlerController
) extends CrawlerServiceStack with CrawlerUriHandlerImpl {

  protected implicit lazy val jsonFormats: Formats = DefaultFormats

  protected implicit def executor = executionContext

  postLogRecover("/") {
    val body = request.body
    val indexRequest = parse(body).extract[IndexRequest]

    for {
      result <- controller.indexPage(indexRequest)
    } yield Created(result)
  }

}

object CrawlerUriHandler {
  val Path = "/index"
}