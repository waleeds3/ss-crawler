package io.github.waleedsamy.sscrawler.crawler

import com.typesafe.config.Config
import io.github.waleedsamy.sscrawler.model._
import io.github.waleedsamy.sscrawler.service.Indexer
import io.github.waleedsamy.sscrawler.shared.{ FutureUtil, Logging }
import org.json4s.{ DefaultFormats, Formats }

import scala.concurrent.{ ExecutionContext, Future }

class CrawlerController(
    indexer: Indexer,
    appConfig: Config,
    executionContext: ExecutionContext
) extends FutureUtil with Logging {

  implicit lazy val jsonFormats: Formats = DefaultFormats

  implicit def executor = executionContext

  def _appConfig = appConfig

  def indexPage(indexRequest: IndexRequest): Future[IndexResponse] = {
    Future { indexer.index(indexRequest) }
    Future { IndexResponse(indexRequest) }
  }

}