package io.github.waleedsamy.sscrawler.shared

import java.net.URL

import io.github.waleedsamy.sscrawler.model.IndexedPage

import scala.util.{ Success, Try }

trait UrlUtil {

  def absoluteLink(link: String, page: IndexedPage): URL = {
    val linkable = Try { new URL(link) }

    linkable match {
      case Success(l) => l
      case _ =>
        val start =
          if (link.startsWith("//")) s"${page.protocol}:"
          else if (link.startsWith("/")) s"${page.protocol}://${page.host}"
          else s"${page.protocol}://${page.host}/"

        new URL(s"$start$link")
    }
  }

  def sameHost(link: String, page: IndexedPage): Boolean =
    absoluteLink(link, page).getHost == page.host

}
