package io.github.waleedsamy.sscrawler.shared

import org.slf4j.LoggerFactory

trait Logging {
  protected lazy val log = LoggerFactory.getLogger(getClass())
}