package io.github.waleedsamy.sscrawler.shared

import scala.concurrent.{ ExecutionContext, Future }

trait FutureUtil {
  protected implicit def executor: ExecutionContext

  protected def toFuture[T](opt: Option[T], ex: => Throwable): Future[T] =
    opt.map(Future.successful).getOrElse(Future.failed(ex))
}
