package io.github.waleedsamy.sscrawler.service

import io.github.waleedsamy.sscrawler.model.{ IndexedPage, SearchRequest, SearchResult }
import io.github.waleedsamy.sscrawler.shared.Logging

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scala.concurrent.{ ExecutionContext, Future }

trait SearchEngine {
  def search(searchRequest: SearchRequest): Future[Seq[SearchResult]]
}

/**
 * Search inside text with trivial @String methods
 * @param storage
 * @param executionContext
 */
class SimplestSearchInTextEver(storage: Storage, executionContext: ExecutionContext) extends SearchEngine with Logging {

  protected implicit def executor = executionContext

  private val anySpecialCharacterExceptSpaceRegex = """[^0-9a-zA-Z_\s]"""

  def search(searchRequest: SearchRequest): Future[Seq[SearchResult]] = {

    val simplifiedText = searchRequest.q.trim.replaceAll(anySpecialCharacterExceptSpaceRegex, "").trim

    @tailrec
    def loop(res: ListBuffer[SearchResult], pages: Iterator[IndexedPage]): Seq[SearchResult] = {
      if (res.size == searchRequest.limit) res
      else if (pages.hasNext) {

        val page = pages.next()
        val indices = simplifiedText.r.findAllMatchIn(page.body.content).map(_.start).toList
        val occurrences = indices.map { index =>
          page.body.content.slice(index - 100, index + 100)
        }

        if (occurrences.nonEmpty) {
          log.info(s"search for ${searchRequest.q} in ${page.uri} return ${occurrences.size} result")
          res.append(SearchResult(page, occurrences))
        }
        loop(res, pages)
      } else res
    }

    val result = loop(ListBuffer.empty[SearchResult], storage.pages)

    // Rank do nothing, all IndexedPages has same rank (1.0)
    Future(result.sortBy(_.rank))
  }
}
