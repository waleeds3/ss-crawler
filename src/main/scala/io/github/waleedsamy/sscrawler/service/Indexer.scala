package io.github.waleedsamy.sscrawler.service

import java.net.URL

import io.github.waleedsamy.sscrawler.model.{ EmptyContent, IndexRequest, IndexedPage, LowerCaseContent }
import io.github.waleedsamy.sscrawler.shared.{ Logging, UrlUtil }
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._

import scala.util.Try

trait Indexer extends Logging {
  val storage: Storage
  def index(indexRequest: IndexRequest, deep: Int = 100): Unit
  def fetch(indexRequest: IndexRequest): Option[IndexedPage]
  def indexed(uri: URL): Boolean = storage.find(uri).isDefined
}

/**
 * SimpleIndexer does not care about robots.txt.
 * Back pressure should used to slow down the indexer when the is no enough space [https://chariotsolutions.com/blog/post/simply-explained-akka-streams-backpressure/]
 *
 */
class SimpleIndexer(val storage: Storage) extends Indexer with UrlUtil with Logging {
  private val browser = JsoupBrowser()

  override def index(indexRequest: IndexRequest, deep: Int) = {
    val uri = indexRequest.getUri
    val alreadyIndexed = indexed(uri)

    if (!alreadyIndexed) {
      val page = fetch(indexRequest)

      page.flatMap(storage.store).map { page =>
        val linksToSameHost = page.links.take(deep).filter(sameHost(_, page))
        linksToSameHost.map(absoluteLink(_, page)).map(new IndexRequest(_, indexRequest.id)).foreach { index(_) }
      }
    } else
      log.debug(s"Job: ${indexRequest.id} - bot been at ${uri.toString} before")
  }

  override def fetch(indexRequest: IndexRequest): Option[IndexedPage] = {

    val uri = indexRequest.getUri

    val docTry = Try(browser.get(uri.toString))

    val page = docTry.map { doc =>
      val text = doc >> allText

      val links = (doc >> "a")
        .filter(_.hasAttr("href"))
        .map(_.attr("href")).toList

      log.info(s"Job: ${indexRequest.id} - Indexing ${indexRequest.site}")

      IndexedPage(
        host = uri.getHost, protocol = uri.getProtocol,
        uri = uri.toString,
        body = new LowerCaseContent(text.toLowerCase),
        links = links
      )

    } getOrElse {

      log.info(s"Job: ${indexRequest.id} - Failed to index ${indexRequest.site}")

      IndexedPage(
        host = uri.getHost, protocol = uri.getProtocol,
        uri = uri.toString,
        body = new EmptyContent
      )
    }

    Some(page)
  }

}
