package io.github.waleedsamy.sscrawler

import io.github.waleedsamy.sscrawler.shared.Logging
import org.scalatra.json.JacksonJsonSupport
import org.scalatra.{ AsyncResult, CorsSupport, FutureSupport, ScalatraServlet }

import scala.concurrent.Future

trait CrawlerServiceStack extends ScalatraServlet with JacksonJsonSupport
    with FutureSupport with CorsSupport with Logging {

  before() {
    contentType = formats("json")
  }

  notFound {
    // remove content type in case it was set through an action
    contentType = null
    serveStaticResource() getOrElse resourceNotFound()
  }

  options("/*") {
    new AsyncResult {
      val is = Future {
        response.setHeader(
          CorsSupport.AccessControlAllowHeadersHeader,
          request.getHeader(CorsSupport.AccessControlRequestHeadersHeader)
        )

        response.setHeader(
          CorsSupport.AccessControlAllowOriginHeader,
          CrawlerServiceStack.AllowedOrigin
        )

        response.setHeader(
          CorsSupport.AccessControlAllowMethodsHeader,
          CrawlerServiceStack.AllowedMethods
        )
      }
    }
  }
}
object CrawlerServiceStack {
  final val AllowedOrigin = "*"
  final val AllowedMethods = "GET,POST,PUT,DELETE,HEAD,OPTIONS,PATCH"
}