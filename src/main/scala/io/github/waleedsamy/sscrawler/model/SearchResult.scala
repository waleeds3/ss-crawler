package io.github.waleedsamy.sscrawler.model

import io.github.waleedsamy.sscrawler.model.SearchResult.SearchContext

case class SearchResult(page: IndexedPage, contexts: Seq[SearchContext]) {
  def rank: Double = 1.0
}

object SearchResult {

  type SearchContext = String
}