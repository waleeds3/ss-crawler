package io.github.waleedsamy.sscrawler.model

case class SearchRequest(q: String, limit: Int = 10)
