package io.github.waleedsamy.sscrawler.model

trait Content {
  def content: String = ""
}

class LowerCaseContent(payload: String) extends Content {
  override val content = payload.toLowerCase
}

class EmptyContent extends Content {
  override val content = ""
}
