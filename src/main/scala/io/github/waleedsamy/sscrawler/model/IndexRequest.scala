package io.github.waleedsamy.sscrawler.model

import java.net.URL
import java.util.UUID

case class IndexRequest(site: String, id: String = UUID.randomUUID().toString) {

  def this(url: URL) = this(url.toString)
  def this(url: URL, id: String) = this(url.toString, id)
  def getUri = new URL(site)
}
