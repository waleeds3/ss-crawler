# Goal

The goal of this challenge is to build a small scale, in-memory web index for HTML web pages.

A web index consists of a crawler, a search engine and a storage.

The crawler's task is given a URL, to scrape the page for the actual content (that is, extracting the main text of the page) and to continue crawling any outgoing links it encounters.

The search engine enables quick queries on the text of the pages that have been crawled before.

# Constraints

## Storage

As the goal is to build an in-memory web index, you can only use the memory of the application itself for storage.
The application must not use the filesystem or external services (e.g. databases) for storage.
While you can assume to have a reasonable amount of heap memory available, let's say 6GB, you should think about how to evict stored data to prevent out-of-memory situations.

## Crawler

Given a URL by the user of the application, the job of the crawler is to scrape the text content of the page and to walk along any outgoing links it encounters and scrape these pages as well.
Because memory is scarce, the crawler should only consider content of the same web page.
Any URLs the crawler encounters, having a different hostname, than the URL that was given to the crawler by the user, must be disregarded and not processed any further.

## Scraping

Don't reinvent the wheel for extracting the text from the pages. Feel free to make use of existing libraries or come up with a simple enough solution to get a decent amount of text from a page.

## Search engine

The search engine component should allow querying the crawled pages by text.
It is up to you to decide on the expressiveness of the query language.

The search engine should at least return:
* A link to the matching page
* An extract of the text that matched the search query including the context (i.e. a few words before and after the match)

## User Interface

The user interface of the final application should allow:
* Providing URLs to the crawler for subsequent processing
* Querying the search engine to retrieve the matching search results

The interface must not block, so crawling should not block the search engine and vice-versa, i.e. searching must be possible while pages are being crawled.

The technical implementation of the interface is up to you. We assume a tech-savvy user, so the interface could be a REPL session - via command line - or an HTTP interface.
You should provide some documentation on how to use the interface.

# Hints

You are allowed to use auxiliary libraries if you need to (e.g. web frameworks, CLI parsing libraries, scraping libraries, etc.).
Feel free to make assumptions if the description of the challenge is unclear.

A test suite should be part of your solution.

Focus on establishing a minimum viable solution first before considering any optimizations or optional features.
It is more important to deliver something that works and that can be improved rather than something that doesn't work.

Once you have viable solution, feel free to incorporate your own ideas.

Have fun!

